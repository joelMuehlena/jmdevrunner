#ifndef BUFFERLISTENERTHREAD_H
#define BUFFERLISTENERTHREAD_H

#include <QObject>
#include <QThread>

#include <memory>

#include "../core/buffer/buffer.h"

class BufferListenerThread : public QThread {
  Q_OBJECT
private:
  std::shared_ptr<Buffer> buffer;
  QString tabName;

public:
  BufferListenerThread(std::shared_ptr<Buffer> buffer, QString tabName);
  void run() override;

signals:
  void receivedText(const QString tabName, const QString s);
};

#endif // BUFFERLISTENERTHREAD_H
