#include "bufferlistenerthread.h"

BufferListenerThread::BufferListenerThread(std::shared_ptr<Buffer> buffer,
                                           QString tabName)
    : buffer(buffer), tabName(tabName) {}

void BufferListenerThread::run() {
  auto consumer = this->buffer->startConsume();

  std::string s;

  do {
    // Read from the pipe
    s.clear();
    consumer->read(&s);
    auto sCpy = QString().fromStdString(s);
    emit this->receivedText(this->tabName, sCpy);
  } while (consumer->isOpen());
}
