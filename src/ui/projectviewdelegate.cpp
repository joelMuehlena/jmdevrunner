#include "projectviewdelegate.h"

ProjectViewDelegate::ProjectViewDelegate(QObject *parent)
    : QStyledItemDelegate(parent) {}

void ProjectViewDelegate::paint(QPainter *painter,
                                const QStyleOptionViewItem &option,
                                const QModelIndex &index) const {
  const ProjectModel *m = dynamic_cast<const ProjectModel *>(index.model());

  if (m != nullptr) {
    /*qDebug() << option;
    qDebug() << index;
    qDebug() << "\n";*/

    Project *p = m->getProject(index.row()).get();
    Q_UNUSED(p);

    if (option.state & QStyle::State_Selected) {
      emit selectedItem(index.row());
    } else if (!(option.state & QStyle::State_Selected)) {
      emit deselectedItem(index.row());
    }
  }

  QStyledItemDelegate::paint(painter, option, index);
}
