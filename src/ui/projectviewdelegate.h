#ifndef PROJECTVIEWDELEGATE_H
#define PROJECTVIEWDELEGATE_H

#include <QDebug>
#include <QStyledItemDelegate>

#include "models/projectmodel.h"

class ProjectViewDelegate : public QStyledItemDelegate {
  Q_OBJECT
public:
  ProjectViewDelegate(QObject *parent = nullptr);
  void paint(QPainter *painter, const QStyleOptionViewItem &option,
             const QModelIndex &index) const override;
signals:
  void selectedItem(int index) const;
  void deselectedItem(int index) const;
};

#endif // PROJECTVIEWDELEGATE_H
