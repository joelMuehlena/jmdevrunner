#include "runningprojects.h"
#include "ui_runningprojects.h"

QRegularExpression RunningProjectsWindow::ansiRegex = QRegularExpression(
    "\\e[ #%()*+\\-.\\/]. | \\r | (?:\\e\\[|\\x9b) [ -?]* [@-~] | "
    "(?:\\e\\]|\\x9d) .*? (?:\\e\\\\|[\\a\\x9c]) | "
    "(?:\\e[P^_]|[\\x90\\x9e\\x9f]) .*? (?:\\e\\\\|\\x9c) | \\e.|[\\x80-\\x9f]",
    QRegularExpression::ExtendedPatternSyntaxOption);

RunningProjectsWindow::RunningProjectsWindow(QWidget *parent)
    : QWidget(parent), ui(new Ui::RunningProjectsWindow) {
  ui->setupUi(this);

  // Setup tab pane
  this->mainLayout = new QVBoxLayout();
  this->setLayout(this->mainLayout);

  this->tabs = new QTabWidget(this->mainLayout->widget());
  this->mainLayout->addWidget(tabs);
}

RunningProjectsWindow::~RunningProjectsWindow() {
  this->terminate();

  delete this->tabs;
  delete this->mainLayout;
  delete this->ui;
}

void RunningProjectsWindow::terminate() {}

void RunningProjectsWindow::addTab(std::shared_ptr<Project> project,
                                   const RunGroup::Command command,
                                   std::shared_ptr<Buffer> buffer) {
  QTextBrowser *text = new QTextBrowser();
  QString tabName =
      project->getName() + ":" + QString().fromStdString(command.name);
  this->tabs->addTab(text, tabName);
  this->tabsContent.insert(tabName, text);

  auto t = new BufferListenerThread(buffer, tabName);
  t->start();
  connect(t, &BufferListenerThread::receivedText, this,
          &RunningProjectsWindow::addWindowContent);
  this->threads.push_back(t);
}

void RunningProjectsWindow::addWindowContent(const QString tabName,
                                             QString content) {

  auto found = this->tabsContent.find(tabName);

  if (found != this->tabsContent.end()) {

    QTextBrowser *textBrowser = dynamic_cast<QTextBrowser *>(found.value());

    if (textBrowser) {
      content = content.replace(ansiRegex, "");
      auto text = textBrowser->toPlainText();
      textBrowser->setPlainText(text + content);
    }
  }
}

void RunningProjectsWindow::removeTab(const QString &name) {
  for (int i = 0; i < this->tabs->count(); ++i) {
    if (this->tabs->tabText(i) == name) {
      this->tabs->removeTab(i);
    }
  }
}

void RunningProjectsWindow::closeEvent(QCloseEvent *event) {
  emit windowClosed();
  QWidget::closeEvent(event);
}
