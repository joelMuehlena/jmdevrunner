#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow), addProjectDialog(nullptr),
      runningProjectsWindow(nullptr), varReplacerWindow(nullptr),
      model(nullptr), projectViewDelegate(nullptr), config(nullptr),
      selectedProject(nullptr) {
  this->model = new ProjectModel();
  this->projectViewDelegate = new ProjectViewDelegate();
  ui->setupUi(this);
  this->loadProjectData();
  this->initProjectListView();
}

MainWindow::~MainWindow() {
  saveProjectData();

  delete addProjectDialog;
  delete runningProjectsWindow;
  delete varReplacerWindow;
  delete model;
  delete projectViewDelegate;
  delete config;
  delete ui;
}

void MainWindow::runProject(std::shared_ptr<Project> p,
                            const RunGroup::Command command) {
  this->runProjects(CMDRunner::RunListType(
      {std::make_tuple(p, command, std::make_shared<Buffer>(1024))}));
}

void MainWindow::runProjects(CMDRunner::RunListType toRun) {
  if (this->runningProjectsWindow == nullptr) {
    this->runningProjectsWindow = new RunningProjectsWindow(nullptr);
    this->runningProjectsWindow->setWindowTitle("Running Projects");
    connect(this->runningProjectsWindow, &RunningProjectsWindow::windowClosed,
            this, [this]() {
              delete this->runningProjectsWindow;
              this->runningProjectsWindow = nullptr;
            });
    this->runningProjectsWindow->show();
  }

  auto varList = CMDRunner::getAllVarNames(toRun);

  if (varList.size() == 0) {
    CMDRunner::run(toRun);

    for (const auto &[project, cmd, buffer] : toRun) {
      this->runningProjectsWindow->addTab(project, cmd, buffer);
    }

    return;
  }

  auto *varReplacerWindow = new VarReplacerWindow(varList);
  connect(
      varReplacerWindow, &VarReplacerWindow::windowClosed, this,
      [this, &toRun](VarReplacerWindow::windowReturnType values, bool abort) {
        if (abort) {
          delete this->runningProjectsWindow;
          this->runningProjectsWindow = nullptr;
        } else {

          auto newHooks = CMDRunner::replaceAllVarNames(toRun, values);

          CMDRunner::run(toRun, newHooks);

          for (const auto &[project, cmd, buffer] : toRun) {
            this->runningProjectsWindow->addTab(project, cmd, buffer);
          }
        }
      });

  varReplacerWindow->exec();
}

void MainWindow::projectSelected(int index) {
  auto p = this->model->getProject(index);
  this->selectedProject = p;
  if (!this->selectedProjects.contains(p)) {
    this->selectedProjects.push_back(p);
    this->ui->projectInfoPain->setText(p->info());
    this->ui->runDefault_btn->setDisabled(false);
    this->ui->removeProject_btn->setDisabled(false);
    setupProjectRunGroups(this->model->getProject(index));
    connect(this->ui->runDefault_btn, &QPushButton::clicked, this,
            [this, p]() { this->runProject(p, p->getDefaultRunCommand()); });
  }
}

void MainWindow::projectDeselected(int index) {
  auto p = this->model->getProject(index);

  if (this->selectedProject == p)
    this->selectedProject = nullptr;

  if (this->selectedProjects.contains(p)) {
    for (auto it = this->selectedProjects.cbegin();
         it != this->selectedProjects.cend(); ++it) {
      if (*it == p) {
        this->selectedProjects.erase(it);
        break;
      }
    }
  }
}

void MainWindow::setupProjectRunGroups(std::shared_ptr<Project> p) {

  auto runGroups = p->getRunGroups();

  qDeleteAll(this->ui->commandGroupsScroll->findChildren<QGroupBox *>());
  delete this->ui->commandGroupsScroll->widget()->layout();

  QVBoxLayout *vbox = new QVBoxLayout();
  this->ui->commandGroupsScroll->widget()->setLayout(vbox);

  if (p->getIsGitProject()) {
    createGitGroupBtns(vbox, p->getLocalUrl().isEmpty());
  }

  for (const auto &runGroup : runGroups) {
    QGroupBox *box = new QGroupBox(this->ui->commandGroupsScroll->widget());
    box->setTitle(QString().fromStdString(runGroup.getGroupName()));

    QGridLayout *btnGrid = new QGridLayout();
    box->setLayout(btnGrid);

    int col = 0, row = 0, cnt = 0;
    for (const auto &cmd : runGroup.getCommands()) {
      QPushButton *btn = new QPushButton(QString().fromStdString(cmd.name));
      QString name = QString().fromStdString(runGroup.getGroupName()) + ";" +
                     QString().fromStdString(cmd.name);
      btn->setObjectName(name);
      connect(btn, &QPushButton::clicked, this, [this, p, cmd, runGroup]() {
        this->runProject(
            p, p->getCommand(runGroup.getGroupName() + ";" + cmd.name));
      });
      btn->setToolTip(QString().fromStdString(cmd.cmd));
      btnGrid->addWidget(btn, row, col % 2);
      col++;
      cnt++;
      if (cnt % 2 == 0) {
        row++;
      }
    }

    this->ui->commandGroupsScroll->widget()->layout()->addWidget(box);
  }
}

void MainWindow::createGitGroupBtns(QVBoxLayout *vbox, bool isClone) const {
  QGroupBox *box = new QGroupBox(this->ui->commandGroupsScroll->widget());
  box->setTitle("Git Commands");
  QGridLayout *btnGrid = new QGridLayout();
  box->setLayout(btnGrid);

  QPushButton *fetch = new QPushButton("Fetch");
  btnGrid->addWidget(fetch, 0, 0);
  QPushButton *pull = new QPushButton("Pull");
  btnGrid->addWidget(pull, 0, 1);
  QPushButton *push = new QPushButton("Push");
  btnGrid->addWidget(push, 1, 0);

  if (isClone) {
    QPushButton *clone = new QPushButton("Clone");
    btnGrid->addWidget(clone, 1, 1);
  }

  vbox->addWidget(box);
}

void MainWindow::on_btn_add_clicked() {
  this->addProjectDialog = new AddProjectDialog(this);
  this->addProjectDialog->setWindowTitle("Add Project");
  connect(this->addProjectDialog, &AddProjectDialog::addProject, this,
          &MainWindow::addProject);
  this->addProjectDialog->exec();
  this->addProjectDialog = nullptr;
}

void MainWindow::addProject(std::shared_ptr<Project> p) {
  p.get()->setup();
  this->model->addProject(p);
  this->saveProjectData();
}

void MainWindow::loadProjectData() {

  this->config = new Config("data.jmRunner");
  try {
    config->open();
    auto data = config->getProjectData();

    for (const auto &p : data) {
      this->model->addProject(p);
    }

  } catch (std::runtime_error &err) {
    qDebug() << err.what();
  }
}

void MainWindow::saveProjectData() const {
  std::ofstream oFile("data.jmRunner", std::ios::out | std::ios::binary);

  // 18 Byte start string indicator
  std::string s = "JMRUNNER FILE v0.1";
  oFile.write(s.data(), 18);
  char c = 0;
  oFile.write((char *)&c, sizeof(char));

  if (oFile.good()) {
    wchar_t dataIndicator = 0xFE23;
    oFile.write(reinterpret_cast<char *>(&dataIndicator), sizeof(wchar_t));
    *(this->model) >> oFile;
  } else {
    qDebug() << "File error";
  }

  oFile.close();
}

void MainWindow::initProjectListView() const {
  this->ui->projectsView->setModel(this->model);
  this->ui->projectsView->setItemDelegate(this->projectViewDelegate);
  connect(this->projectViewDelegate, &ProjectViewDelegate::selectedItem, this,
          &MainWindow::projectSelected);
  connect(this->projectViewDelegate, &ProjectViewDelegate::deselectedItem, this,
          &MainWindow::projectDeselected);
  this->ui->projectsView->show();
}

void MainWindow::on_actionSave_triggered() { this->saveProjectData(); }

void MainWindow::on_removeProject_btn_clicked() {
  if (this->selectedProject == nullptr && this->selectedProjects.size() == 0)
    return;

  auto p = this->selectedProjects.size() == 1 ? this->selectedProjects.at(0)
                                              : this->selectedProject;

  int index = this->model->find(p);
  this->model->removeProject(index);
}

void MainWindow::on_btn_run_clicked() {
  CMDRunner::RunListType toRun;
  for (const auto &p : this->selectedProjects) {
    toRun.push_back(std::make_tuple(p, p->getDefaultRunCommand(),
                                    std::make_shared<Buffer>(1024)));
  }
  this->runProjects(toRun);
}
