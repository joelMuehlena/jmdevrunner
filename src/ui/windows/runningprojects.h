#ifndef RUNNINGPROJECTS_H
#define RUNNINGPROJECTS_H

#include <QMap>
#include <QRegularExpression>
#include <QString>
#include <QTabWidget>
#include <QTextBrowser>
#include <QThread>
#include <QVBoxLayout>
#include <QVector>
#include <QWidget>

#include "../../core/project.h"
#include "../../core/rungroup.h"

#include "../bufferlistenerthread.h"

namespace Ui {
class RunningProjectsWindow;
}

class RunningProjectsWindow : public QWidget {
  Q_OBJECT

public:
  explicit RunningProjectsWindow(QWidget *parent = nullptr);
  ~RunningProjectsWindow();

  void addTab(std::shared_ptr<Project> project, const RunGroup::Command command,
              std::shared_ptr<Buffer> buffer);
  void removeTab(const QString &name);

signals:
  void windowClosed();

private slots:
  void addWindowContent(const QString tabName, QString content);

private:
  /**
   *  regex for removing ANSI/VT100/xterm control sequnces
   * \e[ #%()*+\-.\/]. | \r | (?:\e\[|\x9b) [ -?]* [@-~] | (?:\e\]|\x9d) .*?
   * (?:\e\\|[\a\x9c]) | (?:\e[P^_]|[\x90\x9e\x9f]) .*? (?:\e\\|\x9c) |
   * \e.|[\x80-\x9f] /xg
   */
  static QRegularExpression ansiRegex;

  Ui::RunningProjectsWindow *ui;
  QVector<QThread *> threads;
  QVBoxLayout *mainLayout;
  QTabWidget *tabs;

  QMap<QString, QWidget *> tabsContent;
  void closeEvent(QCloseEvent *event) override;

  void terminate();
};

#endif // RUNNINGPROJECTS_H
