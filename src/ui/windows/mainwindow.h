#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QGroupBox>
#include <QLabel>
#include <QMainWindow>
#include <QTabWidget>
#include <QVector>

#include <fstream>
#include <iostream>

#include "../../core/cmdrunner.h"
#include "../../core/config.h"
#include "../../core/project.h"
#include "../models/projectmodel.h"
#include "../projectviewdelegate.h"
#include "addprojectdialog.h"
#include "runningprojects.h"
#include "varreplacerwindow.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

private slots:
  void on_btn_add_clicked();
  void addProject(std::shared_ptr<Project> p);
  void projectSelected(int index);
  void projectDeselected(int index);

  void on_actionSave_triggered();
  void on_removeProject_btn_clicked();

  void on_btn_run_clicked();

private:
  Ui::MainWindow *ui;
  AddProjectDialog *addProjectDialog;
  RunningProjectsWindow *runningProjectsWindow;
  VarReplacerWindow *varReplacerWindow;
  ProjectModel *model;
  ProjectViewDelegate *projectViewDelegate;
  Config *config;
  std::shared_ptr<Project> selectedProject;
  QVector<std::shared_ptr<Project>> selectedProjects;

  void setupProjectRunGroups(std::shared_ptr<Project> p);
  void createGitGroupBtns(QVBoxLayout *vbox, bool isClone) const;

  void runProject(std::shared_ptr<Project> p, const RunGroup::Command command);
  void runProjects(CMDRunner::RunListType toRun);

  void loadProjectData();
  void saveProjectData() const;
  void initProjectListView() const;
};
#endif // MAINWINDOW_H
