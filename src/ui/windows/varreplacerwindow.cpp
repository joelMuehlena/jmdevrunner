#include "varreplacerwindow.h"
#include "ui_VarReplacerWindow.h"
#include <iostream>

VarReplacerWindow::VarReplacerWindow(std::set<std::string> &values,
                                     QWidget *parent)
    : QDialog(parent), ui(new Ui::VarReplacerWindow), mainLayout(nullptr),
      values(windowReturnType()), abort(false) {
  ui->setupUi(this);
  setupUiWithValueFields(values);
  setupControlButtons();
}

VarReplacerWindow::~VarReplacerWindow() {
  delete mainLayout;
  delete ui;
}

void VarReplacerWindow::setupControlButtons() {
  auto *hbox = new QHBoxLayout(this->mainLayout->widget());
  this->mainLayout->addLayout(hbox);

  auto *okBtn = new QPushButton("OK");

  connect(okBtn, &QPushButton::clicked, this, [this]() { this->close(); });

  auto *abortBtn = new QPushButton("Abort");

  connect(abortBtn, &QPushButton::clicked, this, [this]() {
    this->abort = true;
    this->close();
  });

  hbox->addWidget(okBtn);
  hbox->addWidget(abortBtn);
}

void VarReplacerWindow::closeEvent(QCloseEvent *event) {
  emit windowClosed(this->values, this->abort);
  QDialog::closeEvent(event);
}

void VarReplacerWindow::setupUiWithValueFields(std::set<std::string> &values) {
  this->mainLayout = new QVBoxLayout();
  this->setLayout(this->mainLayout);

  auto *box = new QGroupBox("Variables");
  this->mainLayout->addWidget(box);
  auto *vbox = new QVBoxLayout();
  box->setLayout(vbox);

  for (const auto &value : values) {
    auto *label = new QLabel(QString().fromStdString(value));
    auto *edit = new QLineEdit();
    connect(edit, &QLineEdit::textChanged, this,
            [this, value](const QString &text) {
              auto found = this->values.find(value);

              if (found != this->values.end()) {
                found->second = text.toStdString();
              }
            });

    edit->setObjectName(QString().fromStdString(value));
    edit->setPlaceholderText(QString().fromStdString(value));

    vbox->addWidget(label);
    vbox->addWidget(edit);

    this->values.emplace(value, "");
  }
}
