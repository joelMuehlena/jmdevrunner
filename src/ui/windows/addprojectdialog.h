#ifndef ADDPROJECTDIALOG_H
#define ADDPROJECTDIALOG_H

#include <QDialog>
#include <QFileDialog>
#include <iostream>
#include <memory>

#include "../../core/project.h"

namespace Ui {
class AddProjectDialog;
}

class AddProjectDialog : public QDialog {
  Q_OBJECT

public:
  explicit AddProjectDialog(QWidget *parent = nullptr);
  ~AddProjectDialog();

signals:
  void addProject(std::shared_ptr<Project> p);

private slots:
  void on_abort_btn_clicked();
  void on_isLocalCheck_stateChanged(int val);
  void on_openFile_btn_clicked();
  void on_add_btn_clicked();

private:
  Ui::AddProjectDialog *ui;
};
#endif // ADDPROJECTDIALOG_H
