#ifndef FILEREPLACERWINDOW_H
#define FILEREPLACERWINDOW_H

#include <QDialog>
#include <QGridLayout>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <set>
#include <tuple>
#include <vector>

namespace Ui {
class VarReplacerWindow;
}

class VarReplacerWindow : public QDialog {
  Q_OBJECT

public:
  using windowReturnType = std::map<std::string, std::string>;

  explicit VarReplacerWindow(std::set<std::string> &values,
                             QWidget *parent = nullptr);
  ~VarReplacerWindow();

signals:
  void windowClosed(VarReplacerWindow::windowReturnType values,
                    bool abort = false);

private:
  Ui::VarReplacerWindow *ui;
  QVBoxLayout *mainLayout{};

  windowReturnType values;

  bool abort;

  void setupUiWithValueFields(std::set<std::string> &values);
  void setupControlButtons();

  void closeEvent(QCloseEvent *event) override;
};

#endif // FILEREPLACERWINDOW_H
