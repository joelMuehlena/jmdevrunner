#include "addprojectdialog.h"
#include "ui_addprojectdialog.h"

#include "mainwindow.h"

AddProjectDialog::AddProjectDialog(QWidget *parent)
    : QDialog(parent), ui(new Ui::AddProjectDialog) {
  ui->setupUi(this);
  this->ui->openFile_btn->hide();
}

AddProjectDialog::~AddProjectDialog() { delete ui; }

void AddProjectDialog::on_abort_btn_clicked() { this->close(); }

void AddProjectDialog::on_isLocalCheck_stateChanged(int val) {
  if (val != 0) {
    this->ui->openFile_btn->show();
  } else {
    this->ui->openFile_btn->hide();
  }
}

void AddProjectDialog::on_openFile_btn_clicked() {
  QString folder =
      QFileDialog::getExistingDirectory(this, "Select Folder of the project");
  this->ui->urlInput->setText(folder);
}

void AddProjectDialog::on_add_btn_clicked() {
  if (!this->ui->nameInput->text().isEmpty() &&
      !this->ui->urlInput->text().isEmpty()) {
    auto p = std::make_shared<Project>(this->ui->nameInput->text());

    if (this->ui->isLocalCheck->isChecked()) {
      p.get()->setLocalUrl(this->ui->urlInput->text());
    } else {
      p.get()->setGitUrl(this->ui->urlInput->text());
    }
    emit addProject(p);
    this->close();
  }
}
