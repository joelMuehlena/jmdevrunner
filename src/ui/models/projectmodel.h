#ifndef PROJECTMODEL_H
#define PROJECTMODEL_H

#include <QAbstractListModel>
#include <QDebug>
#include <QLabel>
#include <QLayout>
#include <QPushButton>
#include <QVector>

#include <fstream>

#include <memory>

#include "../../core/project.h"

class ProjectModel : public QAbstractListModel {
  Q_OBJECT
private:
  QVector<std::shared_ptr<Project>> projects;

public:
  ProjectModel(QObject *parent = nullptr);

  // override abstract functions
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index,
                int role = Qt::DisplayRole) const override;
  bool insertRow(int row, const QModelIndex &parent = QModelIndex());

  // own functions
  void addProject(std::shared_ptr<Project> p);
  void removeProject(int index);
  int find(std::shared_ptr<Project> p) const;
  std::shared_ptr<Project> getProject(int index) const;

  void serialize(std::ofstream &stream) const;

  friend std::ofstream &operator>>(const ProjectModel &model,
                                   std::ofstream &stream);
};

#endif // PROJECTMODEL_H
