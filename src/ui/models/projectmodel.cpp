#include "projectmodel.h"

ProjectModel::ProjectModel(QObject *parent) : QAbstractListModel(parent) {}

int ProjectModel::rowCount(const QModelIndex & /*parent*/) const {
  return this->projects.size();
}

QVariant ProjectModel::data(const QModelIndex &index, int role) const {

  /*qDebug() << QString("row %1, role %3")
             .arg(index.row()).arg(role);*/

  auto project = this->projects.at(index.row()).get();

  if (role == Qt::DisplayRole) {

    QHBoxLayout *hbox = new QHBoxLayout();
    hbox->addWidget(new QLabel(project->getName()));
    hbox->addWidget(new QPushButton("run"));

    return QString("%1").arg(project->getName());
  }

  return QVariant();
}

void ProjectModel::addProject(std::shared_ptr<Project> p) {
  this->beginInsertRows(QModelIndex(), this->projects.size(),
                        this->projects.size());
  this->projects.push_back(p);
  this->endInsertRows();
}

int ProjectModel::find(std::shared_ptr<Project> p) const {
  for (int i = 0; i < this->projects.size(); ++i) {
    if (this->projects.at(i) == p) {
      return i;
    }
  }
  return -1;
}

void ProjectModel::removeProject(int index) {

  if (index == -1)
    return;

  this->beginRemoveRows(QModelIndex(), index, index);
  this->projects.remove(index);
  this->endRemoveRows();
}

std::shared_ptr<Project> ProjectModel::getProject(int index) const {
  return this->projects.at(index);
}

void ProjectModel::serialize(std::ofstream &stream) const {
  auto projectsSize = this->projects.size();
  stream.write((char *)&projectsSize, sizeof(qsizetype));
  for (auto it = this->projects.begin(); it != this->projects.end(); ++it) {
    Project *p = (*it).get();
    *p >> stream;
  }
}

std::ofstream &operator>>(const ProjectModel &model, std::ofstream &stream) {
  model.serialize(stream);
  return stream;
}
