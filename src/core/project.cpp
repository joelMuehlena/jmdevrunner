#include "project.h"

Project::Project(QString name) : name(name) { this->setup(); }

QString Project::getName() const { return this->name; }

void Project::setGitUrl(const QString &url) { this->gitUrl = url; }

QString Project::getLocalUrl() const { return this->localUrl; }
void Project::setLocalUrl(const QString &url) { this->localUrl = url; }

bool Project::getIsGitProject() const { return this->isGitProject; }

QString Project::info() const {
  QString s = "";
  s += "Project name: " + name + "\n";
  s += "isGit: " + QVariant(isGitProject).toString() + "\n";
  s += "git url: " + this->getProjectGitUrl() + "\n";
  s += "Project git remotes: " + ([](QVector<QString> list) -> const QString {
         QString s = QString();
         for (int i = 0; i < list.size(); ++i) {
           s.append(list.at(i));
           if (i < s.size() - 1)
             s.append(", ");
         }
         return s;
       })(this->getRemoteUrls()) +
       "\n";
  s += "Project FileUrl: " + localUrl + "\n";

  return s;
}

std::vector<RunGroup> Project::getRunGroups() const {
  auto runGroups = std::vector<RunGroup>();

  if (!this->hasLocalConfigFile())
    return runGroups;

  auto commands = (*runnerConfig.get())["commands"];

  if (!commands.is_null() && commands.is_object()) {
    for (const auto &[gKey, gValue] : commands.items()) {
      RunGroup group = RunGroup(gKey);

      if (gValue.is_object()) {
        for (const auto &[cKey, cValue] : gValue.items()) {
          RunGroup::Command c;
          c.name = cKey;
          c.project = shared_from_this();

          if (cValue.is_string()) {
            c.cmd = cValue;
          } else if (cValue.is_object()) {
            if (!cValue["cmd"].is_null())
              c.cmd = cValue["cmd"];
            if (!cValue["args"].is_null() && cValue["args"].is_array()) {
              for (const auto &arg : cValue["args"]) {
                if (arg.is_string())
                  c.args.push_back(arg);
              }
            }
            if (cValue["prehooks"].is_array()) {
              for (const auto &hook : cValue["prehooks"]) {
                if (hook.is_string())
                  c.prehooks.push_back(hook);
              }
            }
            if (cValue["posthooks"].is_array()) {
              for (const auto &hook : cValue["posthooks"]) {
                if (hook.is_string())
                  c.posthooks.push_back(hook);
              }
            }
          }

          group.addCommand(c);
        }
      }
      runGroups.push_back(group);
    }
  }
  return runGroups;
}

std::pair<std::vector<Hook>, std::vector<Hook>> Project::getHooks() const {
  auto p = std::make_pair<std::vector<Hook>, std::vector<Hook>>(
      std::vector<Hook>(), std::vector<Hook>());

  if (!this->hasLocalConfigFile())
    return p;

  auto parseHooks = [this](json hooks) -> std::vector<Hook> {
    if (hooks.is_null() || !hooks.is_array())
      throw std::runtime_error("please provide valid hook field");

    auto hooksVec = std::vector<Hook>();

    for (const auto &hook : hooks) {
      auto hookName = hook["hook"];
      if (hookName.is_string()) {
        Hook h = Hook(hookName.get<std::string>(), this);
        if (hook["args"].is_object()) {
          for (auto &[key, value] : hook["args"].items()) {
            if (value.is_string()) {
              h.addArg(key, value.get<std::string>());
            } else if (value.is_array()) {
              auto values = std::vector<std::string>(value.size());
              for (int i = 0; i < value.size(); ++i) {
                if (value.at(i).is_string()) {
                  values.at(i) = value.at(i);
                }
              }
              h.addArg(key, values);
            }
          }
        }
        hooksVec.push_back(h);
      }
    }

    return hooksVec;
  };

  auto hooks = (*runnerConfig.get())["prehooks"];

  p.first = parseHooks(hooks);

  hooks = (*runnerConfig.get())["posthooks"];

  p.second = parseHooks(hooks);

  return p;
}

RunGroup::Command Project::getDefaultRunCommand() const {

  if (!this->hasLocalConfigFile())
    return RunGroup::Command();

  auto defaultRunCmd = (*runnerConfig.get())["defaultRun"];

  if (!defaultRunCmd.is_string())
    throw std::runtime_error("no valid defaultRun provided");

  for (const auto &runGroup : this->getRunGroups()) {
    auto cmd = defaultRunCmd.get<std::string>();
    int delIndex = cmd.find(":");
    if (runGroup.getGroupName() == cmd.substr(0, delIndex)) {
      for (const auto &rgCmd : runGroup.getCommands()) {
        if (rgCmd.name == cmd.substr(delIndex + 1)) {
          return rgCmd;
        }
      }
    }
  }

  throw std::runtime_error("no matching runner found");
}

RunGroup::Command Project::getCommand(const std::string &value) const {
  auto runGroups = this->getRunGroups();

  int delIndex = value.find(";");
  std::string runGroupName = value.substr(0, delIndex);
  std::string cmdName = value.substr(delIndex + 1);

  for (const auto &runGroup : runGroups) {
    if (runGroup.getGroupName() == runGroupName) {
      for (const auto &cmd : runGroup.getCommands()) {
        if (cmd.name == cmdName) {
          return cmd;
        }
      }
    }
  }

  throw std::runtime_error("this command does not exist");
}

QString Project::getProjectGitUrl() const {
  if (!this->gitUrl.isEmpty())
    return this->gitUrl;

  if (this->localGitRepo == nullptr)
    return "";

  auto remotes = this->localGitRepo->remotes();

  for (size_t i = 0; i < remotes.count(); ++i) {
    if (std::strcmp(remotes[i], "origin") == 0) {
      return this->localGitRepo->remote(remotes[i]).url();
    }
  }

  return "";
}

QVector<QString> Project::getRemoteUrls() const {
  QVector<QString> urls;

  if (this->localGitRepo == nullptr)
    return urls;

  auto remotes = this->localGitRepo->remotes();

  for (size_t i = 0; i < remotes.count(); ++i) {
    auto remote = this->localGitRepo->remote(remotes[i]);
    urls.push_back(remote.url());
  }

  return urls;
}

void Project::writeString(const QString &s, std::ofstream &stream) const {
  try {
    auto str = s.toStdString();
    auto size = str.size();
    auto marker = Config::loadMarker("END_STRING_MARKER", false);
    stream.write(str.data(), size);
    stream.write((char *)&marker->second.value, sizeof(int));
  } catch (std::runtime_error &err) {
    qDebug() << "Cannot find end string marker - " << err.what();
  }
}

void Project::serialize(std::ofstream &stream) const {
  this->writeString(this->name, stream);
  this->writeString(this->gitUrl, stream);
  this->writeString(this->localUrl, stream);
}

void Project::deserialize(const Config &config) {

  auto marker = Config::loadMarker("END_STRING_MARKER", false);
  auto mValue = marker->second.value;

  FileBuffer *buf = const_cast<FileBuffer *>(config.getFileBuffer());

  auto s = buf->readString(mValue);
  this->name = QString().fromStdString(s);

  s = buf->readString(mValue);
  this->gitUrl = QString().fromStdString(s);

  s = buf->readString(mValue);
  this->localUrl = QString().fromStdString(s);

  this->setup();
}

std::ofstream &operator>>(const Project &project, std::ofstream &stream) {
  project.serialize(stream);
  return stream;
}

Project *Project::setup() {
  if (!this->localUrl.isEmpty()) {
    this->checkForGitProject();
    auto types =
        ProjectType::determineProjectTypes(this->localUrl.toStdString());
    this->findRunnerConfig();
  }

  return this;
}

bool Project::findRunnerConfig() {

  if (this->localUrl.isEmpty()) {
    this->runnerConfig = nullptr;
    return false;
  };

  using std::filesystem::directory_iterator;
  std::filesystem::path p(this->localUrl.toStdString());
  std::filesystem::directory_iterator end_itr;

  for (directory_iterator itr(p); itr != end_itr; ++itr) {
    if (is_regular_file(itr->path())) {
      std::string fileName = itr->path().filename().string();
      std::cout << fileName << std::endl;

      const char *configFileName = "jmDevRunner.config.json";
      if (fileName == configFileName) {
        p.append(configFileName);
        std::cout << p.string() << std::endl;
        std::ifstream jsonConfig(p, std::ios::in);

        if (!jsonConfig)
          throw new std::runtime_error("error opening dev runner config for " +
                                       p.string());

        this->runnerConfig = std::make_unique<json>();

        try {
          jsonConfig >> *this->runnerConfig.get();
        } catch (std::exception &e) {
          qDebug() << e.what();
        }

        jsonConfig.close();
        return true;
      }
    }
  }

  this->runnerConfig = nullptr;
  return false;
}

bool Project::hasLocalConfigFile() const {
  return this->runnerConfig != nullptr;
}

Project *Project::checkForGitProject() {

  this->isGitProject = false;

  if (this->localUrl.isEmpty())
    return this;

  try {
    this->localGitRepo =
        std::make_unique<git::Repository>(this->localUrl.toStdString());
    this->isGitProject = true;
  } catch (std::exception &e) {
    qDebug() << e.what();
  }

  return this;
}
