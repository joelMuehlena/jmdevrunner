#ifndef FILEBUFFER_H
#define FILEBUFFER_H

#include <cstdint>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

/**
 * @brief The FileBuffer class which handles the reading of a binary file
 */
class FileBuffer {
public:
  enum class Endian { BIG, LITTLE };

private:
  std::vector<uint8_t> buffer;
  size_t filePointer;
  Endian endian;

  bool isPointerInFile() const;

public:
  FileBuffer();
  explicit FileBuffer(std::ifstream &ifile);

  void setEndian(Endian endian);
  Endian getEndian() const;

  std::string readString(size_t length);
  std::string readString(int endMarker);
  std::string readString(char delimiter);
  uint8_t *read(size_t length);
  uint8_t read();
  uint8_t readAt(size_t pos);
  uint8_t peek();

  void resetFilePointer();
  void setFilePointer(size_t value);
  size_t getFilePointer() const;

  size_t size() const;
  bool isEnd() const;

  static int createIntFromBufferValues(Endian endian, int *values, size_t size);
};

#endif // FILEBUFFER_H
