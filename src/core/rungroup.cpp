#include "rungroup.h"

#include "hooks/varreplacer.h"
#include "project.h"

using RGCommand = RunGroup::Command;

RunGroup::RunGroup() : groupName(""), commands(std::vector<Command>()) {}

RunGroup::RunGroup(const std::string &groupName) : groupName(groupName) {}

RunGroup::RunGroup(const std::string &groupName,
                   std::initializer_list<Command> commands)
    : groupName(groupName) {
  for (const auto &c : commands) {
    this->commands.push_back(c);
  }
}

RGCommand::Command()
    : project(nullptr), name(""), cmd(""), args(std::vector<std::string>()) {}
RGCommand::Command(const std::string &name, const std::string &cmd,
                   std::shared_ptr<const Project> project)
    : project(project), name(name), cmd(cmd), args(std::vector<std::string>()) {
}

void RGCommand::run(const Command *pCmd,
                    std::pair<std::vector<Hook>, std::vector<Hook>> hooks,
                    const std::shared_ptr<Buffer> buffer) {
  // execute pre hooks
  for (const auto &ph : pCmd->prehooks) {
    auto builder = HookBase::getHookBuilder(ph);
    auto hookBase = builder();

    for (const auto &hook : hooks.first) {
      if (hook.getName() == ph) {
        hookBase->execute(&hook);
      }
    }
  }

  std::unique_ptr<::Command> cmd;

#ifdef _WIN32
  cmd = std::make_unique<WindowsCommand>(pCmd->cmd, pCmd->project);
#endif

#ifdef linux
  cmd = std::make_unique<LinuxCommand>(this->cmd,
                                       std::shared_ptr<const Project>(project));
#endif

  cmd->setBuffer(buffer);
  auto code = cmd->exec();

  std::cout << "Exec cmd with code: " << code << std::endl;

  // execute post hooks
}

std::string RunGroup::getGroupName() const { return this->groupName; }

const std::vector<RGCommand> &RunGroup::getCommands() const {
  return this->commands;
}

RGCommand RunGroup::getCommand(size_t index) const {
  if (index >= this->commands.size())
    throw std::runtime_error("index is bigger than size");

  return this->commands.at(index);
}

int RunGroup::size() const { return this->commands.size(); }

RunGroup &RunGroup::addCommand(Command c) {
  this->commands.push_back(c);
  return *this;
}

RGCommand RunGroup::operator[](size_t index) const {
  return this->getCommand(index);
}
