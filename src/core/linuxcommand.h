#ifndef LINUXCOMMAND_H
#define LINUXCOMMAND_H

#include "command.h"

class LinuxCommand : public Command {
public:
  LinuxCommand() = delete;
  explicit LinuxCommand(std::string cmd,
                        std::shared_ptr<const Project> project);
  int exec() override;
};

#endif // LINUXCOMMAND_H
