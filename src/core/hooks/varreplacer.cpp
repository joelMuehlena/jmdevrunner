#include "varreplacer.h"
#include "hookbase.h"

namespace VarReplacer {

std::vector<std::string> varParser(std::string arg) {
  auto vars = std::vector<std::string>();
  getVar(0, arg, vars);
  return vars;
}

std::string varReplacer(std::string text, const std::string &varName,
                        const std::string &value) {
  auto position = findVarByName(0, text, varName);

  if (std::get<0>(position) == -1) {
    throw std::runtime_error("var not found");
  }

  text.replace(std::get<0>(position) - 2,
               (std::get<1>(position) - (std::get<0>(position) - 3)), value);
  return text;
}

std::string varReplacerAll(std::string text, const std::string &varName,
                           const std::string &value) {
  auto position = findVarByName(0, text, varName);

  if (std::get<0>(position) != -1) {
    text.replace(std::get<0>(position) - 2,
                 (std::get<1>(position) - (std::get<0>(position) - 3)), value);
  }

  return text;
}

namespace {
std::string getVar(size_t index, std::string &text,
                   std::vector<std::string> &saver) {
  auto varOpening = text.find("${", index);

  if (varOpening != std::string::npos)
    text = getVar(varOpening + 2, text, saver);

  const auto varClosing = text.find('}');
  if (varClosing != std::string::npos && text.at(varClosing - 1) != '\\') {
    std::string varName = text.substr(index, (varClosing - index));
    text.replace(index - 2, (varClosing - (index - 3)), "");
    saver.push_back(varName);
  }

  return text;
}

std::tuple<size_t, size_t> findVarByName(size_t index, const std::string &text,
                                         const std::string &varName) {

  auto result = std::make_tuple(-1, -1);

  auto varOpening = text.find("${", index);

  if (varOpening != std::string::npos) {
    result = findVarByName(varOpening + 2, text, varName);
    if (std::get<0>(result) != -1)
      return result;
  }

  const auto varClosing = text.find('}');
  if (varClosing != std::string::npos && text.at(varClosing - 1) != '\\') {
    std::string foundName = text.substr(index, (varClosing - index));
    if (foundName == varName) {
      std::get<0>(result) = index;
      std::get<1>(result) = varClosing;
    }
  }

  return result;
}
} // namespace

} // namespace VarReplacer