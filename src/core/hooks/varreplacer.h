#ifndef VARREPLACER_H
#define VARREPLACER_H

#include <string>
#include <tuple>
#include <vector>

namespace VarReplacer {
std::vector<std::string> varParser(std::string arg);
std::string varReplacer(std::string text, const std::string &varName,
                        const std::string &value);
std::string varReplacerAll(std::string text, const std::string &varName,
                           const std::string &value);

namespace {
std::string getVar(size_t index, std::string &text,
                   std::vector<std::string> &saver);
std::tuple<size_t, size_t> findVarByName(size_t index, const std::string &text,
                                         const std::string &varName);
} // namespace
} // namespace VarReplacer

#endif // VARREPLACER_H
