#ifndef HOOKBASE_H
#define HOOKBASE_H

#include <functional>
#include <map>
#include <memory>
#include <string>

#include "../hook.h"

class HookBase {
private:
  static std::map<std::string, std::function<std::unique_ptr<HookBase>()>>
      registeredHooks;

  // static std::string getVar(size_t index, std::string& text,
  // std::vector<std::string> &saver); static std::tuple<size_t, size_t>
  // findVarByName(size_t index, const std::string& text, const std::string&
  // varName);

protected:
  std::string hookName;
  std::string hookDescription;

public:
  static void
  registerHook(const std::string &hookName,
               const std::function<std::unique_ptr<HookBase>()> &builder);
  static std::function<std::unique_ptr<HookBase>()>
  getHookBuilder(const std::string &hookName);

  HookBase() = delete;
  explicit HookBase(std::string hookName, std::string hookDescription);
  virtual ~HookBase();

  virtual void execute(const Hook *hook) = 0;

  // static std::vector<std::string> varParser(std::string arg);
  // static std::string varReplacer(std::string text, const std::string&
  // varName, const std::string &value);
};

#endif // HOOK_H
