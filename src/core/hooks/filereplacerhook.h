#ifndef FILEREPLACERHOOK_H
#define FILEREPLACERHOOK_H

#include <QObject>
#include <iostream>

#include "../../ui/windows/varreplacerwindow.h"
#include "../filereplacer.h"
#include "hookbase.h"

class FileReplacerHook : public QObject, public HookBase {
  Q_OBJECT
private:
  const Hook *hook = nullptr;

  void replace(std::map<std::string, std::string> &values);
  void receivedValues();

public:
  FileReplacerHook();

  void execute(const Hook *hook) override;
  static std::unique_ptr<HookBase> build();
};

#endif // FILEREPLACERHOOK_H
