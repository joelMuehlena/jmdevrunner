#include "filereplacerhook.h"

#include <utility>

FileReplacerHook::FileReplacerHook()
    : HookBase("FileReplacer", "A pre hook to replace a regex with an value") {}

void FileReplacerHook::execute(const Hook *hook) {
  std::cout << "execute hook:" << hook->getName() << std::endl;

  this->hook = hook;
  auto values = hook->getArgs();

  auto replaceArg = values.find("replace");
  auto withArg = values.find("with");
  auto excludeArg = this->hook->getArg("exclude");

  if (replaceArg == values.end() || withArg == values.end()) {
    throw std::runtime_error("replace and with attributes are required");
  }

  FileReplacer f;
  f.replace(this->hook->getProjectRef(),
            std::get<const std::string>(replaceArg->second),
            std::get<const std::string>(withArg->second), excludeArg);
}

void FileReplacerHook::receivedValues() {
  std::cout << "Values received" << std::endl;
}

std::unique_ptr<HookBase> FileReplacerHook::build() {
  return std::make_unique<FileReplacerHook>();
}

void FileReplacerHook::replace(std::map<std::string, std::string> &values) {

  auto replaceArg = values.find("replace");
  auto withArg = values.find("with");
  auto excludeArg = this->hook->getArg("exclude");

  if (replaceArg == values.end() || withArg == values.end()) {
    throw std::runtime_error("replace and with attributes are required");
  }

  FileReplacer f;
  f.replace(this->hook->getProjectRef(), replaceArg->second, withArg->second,
            excludeArg);
}
