#include "config.h"

using std::cout, std::endl;

Config::Config(const std::string &pathToConfig)
    : pathToConfig(pathToConfig), isOpened(false) {}

const FileBuffer *Config::getFileBuffer() const { return this->fb.get(); }

void Config::open() {
  auto iFile = std::ifstream("data.jmRunner", std::ios::in | std::ios::binary);

  if (!iFile.good()) {
    throw std::runtime_error("Error reading config file");
  }

  this->fb = std::make_unique<FileBuffer>(iFile);
  iFile.close();

  if (this->fb.get()->size() < Config::RUNNER_FILE_OPENER.size()) {
    throw std::runtime_error("File is not a valid runner file");
  }

  std::string indexString;

  for (std::string::size_type i = 0; i < Config::RUNNER_FILE_OPENER.size();
       ++i) {
    indexString += this->fb.get()->read();
  }

  if (indexString == Config::RUNNER_FILE_OPENER) {
    cout << "Runner file loaded" << endl;
    isOpened = true;
  } else {
    throw std::runtime_error("File is not a valid runner file");
  }

  int endian = this->fb.get()->read();
  if (endian == 0)
    this->fb.get()->setEndian(FileBuffer::Endian::LITTLE);
  else
    this->fb.get()->setEndian(FileBuffer::Endian::BIG);

  this->refreshMarkers();
}

void Config::refreshMarkers() {
  /*for(size_t i = Config::RUNNER_FILE_OPENER.size() + (sizeof (char)); i <
  this->fb.get()->size();++i) { if(i != this->fb.get()->size() - 1) {

         auto m1 = this->fb.get()->read();
         auto m2 = this->fb.get()->read();

         int code;
         if(this->fb.get()->getEndian() == FileBuffer::Endian::LITTLE) {
             code = (m2 << 8) | m1;
         }else {
             code = (m1 << 8) | m2;
         }

         for(auto &marker : this->markers) {
             if(code == marker.second.value && !marker.second.ingore) {
                cout << "Found marker: " << marker.second.name << " at pos: " <<
  i + sizeof (char) *2 << endl; marker.second.pos =  i + sizeof (char) *2;
             }
         }

     }
  }*/

  while (!this->fb.get()->isEnd()) {
    if (this->fb.get()->getFilePointer() + 1 < this->fb.get()->size()) {
      auto m2 = this->fb.get()->peek();
      auto m1 = this->fb.get()->read();

      int code;
      if (this->fb.get()->getEndian() == FileBuffer::Endian::LITTLE) {
        code = (m2 << 8) | m1;
      } else {
        code = (m1 << 8) | m2;
      }

      for (auto &marker : this->markers) {
        if (code == marker.second.value && !marker.second.ingore) {
          cout << "Found marker: " << marker.second.name
               << " at pos: " << this->fb.get()->getFilePointer() + 1 << endl;
          marker.second.pos = this->fb.get()->getFilePointer() + 1;
        }
      }
    }
  }
}

void Config::close() {
  isOpened = false;
  this->fb = nullptr;
}

std::map<std::string, Config::Marker>::iterator
Config::loadMarker(const std::string &name, bool isPosRelevant) {
  std::map<std::string, Marker>::iterator isFound = markers.find(name);

  if (isFound == markers.end()) {
    throw std::runtime_error("No marker with this name");
  }

  if (isFound->second.pos == 0 && isPosRelevant) {
    throw std::runtime_error("No position for this marker");
  }

  return isFound;
}

std::vector<std::shared_ptr<Project>> Config::getProjectData() const {

  if (!isOpened)
    throw std::runtime_error("No config loaded");

  try {
    auto res = this->loadMarker("PROJECT_DATA_MARKER");
    std::vector<std::shared_ptr<Project>> vec;

    cout << "Load Project Data" << endl;

    int values[sizeof(qsizetype)];

    this->fb->setFilePointer(res->second.pos);
    // TODO Exception if size = 0
    for (size_t i = 0; i < sizeof(qsizetype); ++i) {
      values[i] = (int)this->fb.get()->read();
    }

    int size = FileBuffer::createIntFromBufferValues(
        this->fb.get()->getEndian(), values, sizeof(qsizetype));
    cout << "Projects Size: " << size << endl;

    for (int i = 0; i < size; ++i) {
      auto project = std::make_shared<Project>();
      project.get()->deserialize(*this);
      vec.push_back(project);
    }

    return vec;
  } catch (std::runtime_error &err) {
    throw err;
  }
}
