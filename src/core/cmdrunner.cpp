#include "cmdrunner.h"

#include "hooks/varreplacer.h"
#include <thread>
#include <variant>

namespace CMDRunner {

namespace {
void runCMDInThread(const RunGroup::Command &command,
                    std::pair<std::vector<Hook>, std::vector<Hook>> &hooks,
                    const std::shared_ptr<Buffer> buffer) {
  std::thread t = std::thread([hooks, command, buffer]() {
    RunGroup::Command::run(&command, hooks, buffer);
  });
  t.detach();
}
} // namespace

void run(RunListType &toRun) {
  for (const auto &[project, cmd, buffer] : toRun) {
    auto hooks = project->getHooks();

    runCMDInThread(cmd, hooks, buffer);
  }
}

std::set<std::string> getAllVarNames(RunListType &toRun) {
  std::set<std::string> uniqueHookVars;

  for (const auto &[project, cmd, _] : toRun) {
    auto hooks = project->getHooks();

    for (const auto &hook : hooks.first) {
      for (const auto &projectHook : cmd.prehooks) {
        if (hook.getName() == projectHook) {
          for (const auto &arg : hook.getArgs()) {
            std::visit(
                [&uniqueHookVars](auto &&argType) {
                  using T = std::decay_t<decltype(argType)>;
                  if constexpr (std::is_same_v<T, std::string>) {
                    auto varNames = VarReplacer::varParser(argType);
                    uniqueHookVars.insert(varNames.begin(), varNames.end());
                  }
                },
                arg.second);
          }
        }
      }
    }
  }

  return uniqueHookVars;
}

void run(RunListType &toRun,
         std::map<std::shared_ptr<Project>,
                  std::pair<std::vector<Hook>, std::vector<Hook>>>
             newHooks) {
  for (const auto &[project, cmd, buffer] : toRun) {
    auto found = newHooks.find(project);

    if (found == newHooks.end())
      continue;

    runCMDInThread(cmd, found->second, buffer);
  }
}

std::map<std::shared_ptr<Project>,
         std::pair<std::vector<Hook>, std::vector<Hook>>>
replaceAllVarNames(RunListType &toRun,
                   std::map<std::string, std::string> values) {

  std::map<std::shared_ptr<Project>,
           std::pair<std::vector<Hook>, std::vector<Hook>>>
      newHooks;

  for (const auto &[varToReplace, varReplaceWith] : values) {
    for (const auto &[project, cmd, _] : toRun) {

      auto projectMapIt = newHooks.find(project);

      if (projectMapIt == newHooks.end()) {
        newHooks.emplace(
            project, std::make_pair(std::vector<Hook>(), std::vector<Hook>()));
      }

      auto hooks = project->getHooks();
      for (auto &hook : hooks.first) {
        for (const auto &projectHook : cmd.prehooks) {
          if (hook.getName() == projectHook) {
            for (const auto &arg : hook.getArgs()) {
              if (auto argType = std::get_if<const std::string>(&arg.second)) {
                auto newArg = VarReplacer::varReplacerAll(
                    *argType, varToReplace, varReplaceWith);
                projectMapIt = newHooks.find(project);

                bool found = false;

                for (auto &internalHook : projectMapIt->second.first) {
                  if (internalHook.getName() == hook.getName()) {
                    if (newArg != *argType)
                      internalHook.replaceArg(arg.first, newArg);
                    found = true;
                    break;
                  }
                }

                if (!found) {
                  Hook hc = hook;
                  if (newArg != *argType)
                    hc.replaceArg(arg.first, newArg);
                  projectMapIt->second.first.push_back(hc);
                }
              }
            }
            break;
          }
        }
      }
    }
  }

  return newHooks;
}
} // namespace CMDRunner
