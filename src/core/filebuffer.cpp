#include "filebuffer.h"

FileBuffer::FileBuffer() : buffer(std::vector<uint8_t>()), filePointer(0) {}

FileBuffer::FileBuffer(std::ifstream &iFile)
    : buffer(std::vector<uint8_t>()), filePointer(0), endian(Endian::LITTLE) {
  this->buffer.clear();
  this->buffer = std::vector<uint8_t>(std::istreambuf_iterator<char>(iFile),
                                      std::istreambuf_iterator<char>());
}

void FileBuffer::setEndian(Endian endian) { this->endian = endian; }

FileBuffer::Endian FileBuffer::getEndian() const { return this->endian; }

std::string FileBuffer::readString(size_t length) {
  std::string s = "";
  size_t cnt = this->filePointer + length;
  while (this->filePointer < cnt) {
    s += this->buffer.at(this->filePointer);
    ++this->filePointer;
  }

  return s;
}

bool FileBuffer::isEnd() const {
  return this->filePointer >= this->buffer.size() - 1;
}

std::string FileBuffer::readString(int endMarker) {
  std::string s = "";
  while (true) {

    if (!this->isPointerInFile()) {
      throw std::runtime_error("pointer not in file");
    }

    if (this->filePointer + 1 >= this->buffer.size()) {
      break;
    }

    int m1 = this->buffer.at(this->filePointer);
    int m2 = this->buffer.at(this->filePointer + 1);
    int vals[2] = {m1, m2};
    int code = FileBuffer::createIntFromBufferValues(this->endian, vals, 2);
    if (code == endMarker) {
      this->filePointer += sizeof(char) * 4;
      break;
    } else {
      // qDebug() << buffer.at(pos);
      s += buffer.at(this->filePointer);
      this->filePointer++;
    }
  }
  return s;
}

std::string FileBuffer::readString(char delimiter) {
  std::string s = "";

  char c;
  while (true) {

    if (!this->isPointerInFile()) {
      throw std::runtime_error("pointer not in file");
    }

    c = this->buffer.at(this->filePointer);
    ++this->filePointer;

    if (c == delimiter)
      break;
    s += c;
  };

  return s;
}

uint8_t FileBuffer::readAt(size_t pos) { return this->buffer.at(pos); }

uint8_t FileBuffer::peek() {

  if (!this->isPointerInFile()) {
    throw std::runtime_error("pointer not in file");
  }

  if (this->filePointer + 1 > this->buffer.size()) {
    throw std::runtime_error("peek not in file");
  }

  return this->buffer.at(this->filePointer + 1);
}

uint8_t *FileBuffer::read(size_t length) {
  uint8_t *c = new uint8_t[length];

  for (size_t i = 0; i < length; ++i) {

    if (!this->isPointerInFile()) {
      throw std::runtime_error("pointer not in file");
    }

    c[i] = this->buffer.at(this->filePointer);
    ++this->filePointer;
  }

  return c;
}

bool FileBuffer::isPointerInFile() const { return !isEnd(); }

uint8_t FileBuffer::read() {

  if (!this->isPointerInFile()) {
    throw std::runtime_error("pointer not in file");
  }

  return this->buffer.at(this->filePointer++);
}

void FileBuffer::resetFilePointer() { this->filePointer = 0; }
void FileBuffer::setFilePointer(size_t value) { this->filePointer = value; }
size_t FileBuffer::getFilePointer() const { return this->filePointer; }

size_t FileBuffer::size() const { return this->buffer.size(); }

int FileBuffer::createIntFromBufferValues(Endian endian, int *values,
                                          size_t size) {
  int s = 0;
  if (endian == Endian::LITTLE) {
    for (long long i = size - 1; i >= 0; --i) {
      int shiftedVal = values[i] << (i * 8);
      s |= shiftedVal;
    }
  } else {
    for (size_t i = 0; i < size; ++i) {
      int shiftedVal = values[i] << (i * 8);
      s |= shiftedVal;
    }
  }

  return s;
}
