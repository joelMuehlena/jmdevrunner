#include "windowscommand.h"
#include "project.h"

WindowsCommand::WindowsCommand(std::string cmd,
                               std::shared_ptr<const Project> project)
    : Command(cmd, project) {}

void WindowsCommand::setupPseudoConsolePipes() {
  if (!CreatePipe(&hPipePTYIn, &hPipeOut, nullptr, 0))
    createWindowsError("PseudoConsoleInput  CreatePipe");

  if (!CreatePipe(&hPipeIn, &hPipePTYOut, nullptr, 0))
    createWindowsError("PseudoConsoleOutput  CreatePipe");
}

HRESULT WindowsCommand::prepareStartupInformation(HPCON hpc,
                                                  STARTUPINFOEXA *si) {
  // Prepare Startup Information structure
  ZeroMemory(si, sizeof(STARTUPINFOEXA));
  si->StartupInfo.cb = sizeof(STARTUPINFOEXA);

  // Discover the size required for the list
  size_t bytesRequired;
  InitializeProcThreadAttributeList(NULL, 1, 0, &bytesRequired);

  // Allocate memory to represent the list
  si->lpAttributeList = (PPROC_THREAD_ATTRIBUTE_LIST)HeapAlloc(
      GetProcessHeap(), 0, bytesRequired);
  if (!si->lpAttributeList) {
    return E_OUTOFMEMORY;
  }

  // Initialize the list memory location
  if (!InitializeProcThreadAttributeList(si->lpAttributeList, 1, 0,
                                         &bytesRequired)) {
    HeapFree(GetProcessHeap(), 0, si->lpAttributeList);
    return HRESULT_FROM_WIN32(GetLastError());
  }

  // Set the pseudoconsole information into the list
  if (!UpdateProcThreadAttribute(si->lpAttributeList, 0,
                                 PROC_THREAD_ATTRIBUTE_PSEUDOCONSOLE, hpc,
                                 sizeof(HPCON), NULL, NULL)) {
    HeapFree(GetProcessHeap(), 0, si->lpAttributeList);
    return HRESULT_FROM_WIN32(GetLastError());
  }

  return S_OK;
}

void WindowsCommand::PipeListener(LPVOID args) {
  ArgsList *list = (ArgsList *)args;

  HANDLE hPipe{list->pipe};
  Buffer *buf = list->buf;

  const DWORD BUFF_SIZE{BUFSIZE};
  char szBuffer[BUFF_SIZE]{};

  DWORD dwBytesRead{};
  BOOL fRead{FALSE};
  do {
    // Read from the pipe
    fRead = ReadFile(hPipe, szBuffer, BUFF_SIZE, &dwBytesRead, NULL);

    // Write to the buffer which distributes it to the consumers
    buf->write(szBuffer, dwBytesRead);
  } while (fRead && dwBytesRead >= 0);
  buf->close();
}

/**
 * @brief PipeWriterTest Just a test function is going to be removed later on
 * @param buffer A pointer to the buffer to consume from
 */
void WindowsCommand::PipeWriterTest(LPVOID buffer) {
  HANDLE hConsole{GetStdHandle(STD_OUTPUT_HANDLE)};

  DWORD dwBytesWritten{};
  Buffer *buf = (Buffer *)buffer;

  auto consumer = buf->startConsume();

  std::string s;

  do {
    // Read from the pipe
    s.clear();
    consumer->read(&s);

    // Write received text to the Console
    // Note: Write to the Console using WriteFile(hConsole...), not
    // printf()/puts() to prevent partially-read VT sequences from corrupting
    // output
    WriteFile(hConsole, s.c_str(), s.length(), &dwBytesWritten, NULL);
  } while (consumer->isOpen());
}

void WindowsCommand::PipeFeeder(LPVOID args) {
  ArgsList *list = (ArgsList *)args;

  HANDLE hPipe{list->pipe};
  HANDLE hConsole{GetStdHandle(STD_OUTPUT_HANDLE)};

  DWORD dwBytesWritten{};

  int len = 5;
  char cmd[] = "start";

  bool write = false;

  do {
    write = WriteFile(hPipe, cmd, len, &dwBytesWritten, NULL);

  } while (false);
}

void WindowsCommand::createPseudConsole(HPCON *hPc) {
  HANDLE hConsole = {GetStdHandle(STD_OUTPUT_HANDLE)};

  this->setupPseudoConsolePipes();

  COORD consoleSize{};
  CONSOLE_SCREEN_BUFFER_INFO csbi{};
  if (GetConsoleScreenBufferInfo(hConsole, &csbi)) {
    consoleSize.X = csbi.srWindow.Right - csbi.srWindow.Left + 1;
    consoleSize.Y = csbi.srWindow.Bottom - csbi.srWindow.Top + 1;
  }

  auto res = CreatePseudoConsole(consoleSize, hPipePTYIn, hPipePTYOut, 0, hPc);
  if (FAILED(res))
    this->createWindowsError("pseudo console");

  if (INVALID_HANDLE_VALUE != hPipePTYOut)
    CloseHandle(hPipePTYOut);
  if (INVALID_HANDLE_VALUE != hPipePTYIn)
    CloseHandle(hPipePTYIn);
}

void WindowsCommand::createWindowsError(const std::string &errorText) {
  DWORD code = GetLastError();
  LPSTR lpMsgBuf;

  if (code == 0)
    return;

  auto size = FormatMessageA(
      FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM |
          FORMAT_MESSAGE_IGNORE_INSERTS,
      NULL, code, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&lpMsgBuf,
      0, NULL);

  std::string msg(lpMsgBuf, size);
  LocalFree(lpMsgBuf);

  throw std::runtime_error(errorText + "()" + std::to_string(code) + ": " +
                           msg);
}

int WindowsCommand::exec() {
  HPCON hPc;
  createPseudConsole(&hPc);

  STARTUPINFOEXA si;
  auto res = this->prepareStartupInformation(hPc, &si);
  if (FAILED(res))
    this->createWindowsError("pseudo console prepare si");

  PROCESS_INFORMATION pi;
  ZeroMemory(&pi, sizeof(pi));

  char *dir = nullptr;

  if (this->project != nullptr) {
    auto n = this->project->getLocalUrl().size() + 1;
    auto nString = this->project->getLocalUrl().replace("/", "\\");
    dir = new char[n];
    std::strncpy(dir, nString.toStdString().c_str(), n);
  }

  std::string cmdString = "/c ";
  cmdString.append(this->cmd);

  char cmdCopy[cmdString.size()];
  cmdString.copy(cmdCopy, cmdString.size());
  cmdCopy[cmdString.size()] = '\0';

  ArgsList *list1 = new ArgsList();
  *list1 = {.pipe = hPipeIn, .buf = this->buf.get()};
  ArgsList *list2 = new ArgsList();
  *list2 = {.pipe = hPipeOut, .buf = this->buf.get()};
  HANDLE hPipeListenerThread{
      reinterpret_cast<HANDLE>(_beginthread(PipeListener, 0, (void *)list1))};
  HANDLE hPipeFeederThread{
      reinterpret_cast<HANDLE>(_beginthread(PipeFeeder, 0, (void *)list2))};
  HANDLE hPipeTestThread{reinterpret_cast<HANDLE>(
      _beginthread(PipeWriterTest, 0, (void *)this->buf.get()))};

  bool rc = CreateProcessA("C:\\Windows\\System32\\cmd.exe", cmdCopy, nullptr,
                           nullptr, false, EXTENDED_STARTUPINFO_PRESENT,
                           nullptr, dir, &si.StartupInfo, &pi);

  delete[] dir;
  if (!rc)
    createWindowsError("Failed to create process");

  WaitForInputIdle(pi.hProcess, INFINITE);
  WaitForSingleObject(pi.hProcess, INFINITE);

  DWORD exitCode{};
  rc = GetExitCodeProcess(pi.hProcess, &exitCode);
  if (!rc)
    createWindowsError("Failed to fetch process exit code");

  this->buf->close();

  /* if (INVALID_HANDLE_VALUE != hPipeListenerThread)
   CloseHandle(hPipeListenerThread); if (INVALID_HANDLE_VALUE !=
   hPipeFeederThread) CloseHandle(hPipeFeederThread); if (INVALID_HANDLE_VALUE
   != hPipeTestThread) CloseHandle(hPipeTestThread);

   if (INVALID_HANDLE_VALUE != pi.hProcess) CloseHandle(pi.hProcess);*/

  return exitCode;
}
