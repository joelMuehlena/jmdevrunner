#ifndef FILEREPLACER_H
#define FILEREPLACER_H

#include <string>
#include <variant>

#include "project.h"

class FileReplacer {
public:
  FileReplacer();

  using excludeVariant =
      std::variant<const std::string, std::vector<std::string>>;

  void replace(const Project *project, const std::string &replaceArg,
               const std::string &withArg, excludeVariant excludeArg) const;
};

#endif // FILEREPLACER_H
