#include "linuxcommand.h"

LinuxCommand::LinuxCommand(std::string cmd,
                           std::shared_ptr<const Project> project)
    : Command(cmd, project) {}

int LinuxCommand::exec() { return 0; }
