#ifndef PROJECT_H
#define PROJECT_H

#include <QDebug>
#include <QString>
#include <QVariant>
#include <QVector>
#include <fstream>
#include <memory>
#include <vector>

#include <filesystem>

#include "git2.h"
#include "git2cpp/repo.h"

#include "nlohmann/json.hpp"

#include "config.h"
#include "projecttype.h"

#include "rungroup.h"

using json = nlohmann::json;

// Forward declaration
class Config;

/**
 * @brief The Project class represents a project in the application
 * @details A Project can be serialized and deserialized for saving and
 * restoring. It holds all neccessary values like the local state and the git
 * info. Not all data is saved to the config. Some info will be refreshed on
 * reload of the application e.g. the git state.
 */
class Project : public std::enable_shared_from_this<Project> {
private:
  QString name;
  QString gitUrl;
  QString localUrl;
  QString configPath;
  std::vector<ProjectType> types;
  bool isGitProject;
  std::unique_ptr<git::Repository> localGitRepo;
  std::unique_ptr<json> runnerConfig;

  /**
   * @brief Checks if a local runner config is present
   * @return If a config was found
   */
  bool findRunnerConfig();

  /**
   * @brief Loads the config at the path
   */
  void loadRunnerConfig();

  /**
   * @brief Creates a default config file for a project
   */
  void createLocalConfig();

  /**
   * @brief Sets the configPath member to its default value.
   * @details The default value is jmDevRunner.config.json
   */
  void setDefaultConfigPath();

  /**
   * @brief Checks if a project is a git project
   * @return A pointer to this to allow chaining
   */
  Project *checkForGitProject();

public:
  inline Project(){};
  explicit Project(QString name);

  /**
   * @brief The basic setup for a project. Should be run before using a project
   * @return A pointer to this to allow chaining
   */
  Project *setup();

  [[nodiscard]] QString getName() const;

  [[nodiscard]] QVector<QString> getRemoteUrls() const;
  [[nodiscard]] QString getProjectGitUrl() const;

  [[nodiscard]] QString getGitUrl() const;
  void setGitUrl(const QString &url);

  [[nodiscard]] QString getLocalUrl() const;
  void setLocalUrl(const QString &url);

  [[nodiscard]] bool getIsGitProject() const;
  [[nodiscard]] bool hasLocalConfigFile() const;
  [[nodiscard]] std::vector<RunGroup> getRunGroups() const;
  [[nodiscard]] std::pair<std::vector<Hook>, std::vector<Hook>>
  getHooks() const;
  [[nodiscard]] RunGroup::Command getDefaultRunCommand() const;

  /**
   * @brief Gets a specific RunGroup::Command in a specific RunGroup
   * @param value The name of the RunGroup and the command. The string must be
   * passed as <runGroup>;<cmd name>
   * @throws std::runtime_error Thrown if no command is found
   * @return A command in a run group
   */
  [[nodiscard]] RunGroup::Command getCommand(const std::string &value) const;

  /**
   * @brief Returns an info string for a project
   * @details It summarizes the important member values into a string
   * @return The info string
   */
  QString info() const;

  void writeString(const QString &s, std::ofstream &stream) const;

  /**
   * @brief Serializes the project into a stream for saving the data
   * @param[in] stream The stream to serialize to
   */
  void serialize(std::ofstream &stream) const;

  /**
   * @brief Serializes the project from a stream for restore saved data
   * @param[in] config A loaded project config which holds the saved data
   */
  void deserialize(const Config &config);

  /**
   * @brief Serializes the project into a stream for saving the data
   * @details Calls the serialize method of a Project
   * @param[in] project The project which is affected by the operator
   * @param[in] stream The stream to serialize to
   * @return
   */
  friend std::ofstream &operator>>(const Project &project,
                                   std::ofstream &stream);
};

#endif // PROJECT_H
