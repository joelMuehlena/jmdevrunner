#include "projecttype.h"

ProjectType::ProjectType() : type(ProjectType::Type::NOT_DEFINED) {}

std::string ProjectType::TypeToString(ProjectType::Type t) {
  switch (t) {
  case Type::NPM:
    return "npm";
  case Type::YARN:
    return "yarn";
  case Type::GO:
    return "go";
  case Type::NOT_DEFINED:
    return "not defined";
  default:
    return "not valid type";
  }
}

std::vector<ProjectType>
ProjectType::determineProjectTypes(const std::string &path) {

  std::cout << "PATH: " << path << std::endl;

  std::vector<ProjectType> types;

  if (path.size() == 0)
    return types;

  using std::filesystem::directory_iterator;
  std::filesystem::path p(path);
  std::filesystem::directory_iterator end_itr;

  try {
    for (directory_iterator itr(p); itr != end_itr; ++itr) {
      if (is_regular_file(itr->path())) {
        std::string fileName = itr->path().filename().string();
        std::cout << fileName << std::endl;
      }
    }
  } catch (std::filesystem::filesystem_error &e) {
    qDebug() << e.what();
  }

  for (auto it = types.cbegin(); it != types.cend(); ++it) {
    if (!it->isCompleted)
      types.erase(it);
  }

  return types;
}
