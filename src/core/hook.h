#ifndef HOOK_H
#define HOOK_H

#include <map>
#include <string>
#include <variant>
#include <vector>

class Project;

class Hook {
public:
  using mapVariantType =
      std::variant<const std::string, std::vector<std::string>>;

private:
  std::string hookName;
  std::map<const std::string, mapVariantType> hookArgs;
  const Project *project;

public:
  Hook() = delete;
  Hook(std::string name, const Project *const project);

  [[nodiscard]] std::string getName() const;
  [[nodiscard]] mapVariantType getArg(const std::string &name) const;
  [[nodiscard]] const std::map<const std::string, mapVariantType> &
  getArgs() const;

  void addArg(const std::string &name, const std::string &arg);
  void addArg(const std::string &name, std::vector<std::string> args);

  void replaceArg(const std::string &name, const mapVariantType &newValue);

  const Project *getProjectRef() const;
};

#endif // HOOK_H
