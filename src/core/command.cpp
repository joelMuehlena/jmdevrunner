#include "command.h"

Command::Command(std::string cmd, std::shared_ptr<const Project> project)
    : exitStatus(0), project(project), cmd(std::move(cmd)),
      buf(std::make_shared<Buffer>(1024)) {}

std::shared_ptr<Buffer> Command::getBuffer() const {
  return std::shared_ptr<Buffer>(this->buf);
}

void Command::setBuffer(std::shared_ptr<Buffer> buffer) { this->buf = buffer; }
