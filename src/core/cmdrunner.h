#ifndef CMDRUNNER_H
#define CMDRUNNER_H

#include "project.h"
#include <map>
#include <memory>
#include <string>
#include <thread>
#include <vector>

namespace CMDRunner {

namespace {
void runCMDInThread(const RunGroup::Command &command,
                    std::pair<std::vector<Hook>, std::vector<Hook>> &hooks,
                    const std::shared_ptr<Buffer> buffer);
}

using RunListType =
    std::vector<std::tuple<std::shared_ptr<Project>, const RunGroup::Command,
                           std::shared_ptr<Buffer>>>;

void run(RunListType &toRun,
         std::map<std::shared_ptr<Project>,
                  std::pair<std::vector<Hook>, std::vector<Hook>>>
             newHooks);
void run(RunListType &toRun);
std::set<std::string> getAllVarNames(RunListType &toRun);
std::map<std::shared_ptr<Project>,
         std::pair<std::vector<Hook>, std::vector<Hook>>>
replaceAllVarNames(RunListType &toRun,
                   std::map<std::string, std::string> values);
} // namespace CMDRunner

#endif // CMDRUNNER_H
