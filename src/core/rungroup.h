#ifndef RUNGROUP_H
#define RUNGROUP_H

#include <QDebug>

#include <initializer_list>
#include <map>
#include <set>
#include <stdexcept>
#include <string>
#include <variant>
#include <vector>

#include "hook.h"
#include "hooks/hookbase.h"

// Include only the code required for the operating system

// For windows system
#ifdef _WIN32
#include "windowscommand.h"
#endif

// for linux systems
#ifdef linux
#include "linuxcommand.h"
#endif

class Project;

/**
 * @brief The RunGroup class holds all run groups of a project with its commands
 * @details Each project can have multiple run groups which can have multiple
 * commands. So it is easier to manage the defined commands
 */
class RunGroup {
public:
  /**
   * @brief The Command class represents a command inside a run group
   * @details This class holds the info about a specefic command inside a
   * project. E.g. the hooks and the command string are managed here
   */
  class Command {
  public:
    Command();

    /**
     * @brief Constucts a command with parameters
     * @param name The name of the command
     * @param cmd The command string to be executed
     * @param project The project this command is part of
     */
    explicit Command(const std::string &name, const std::string &cmd,
                     std::shared_ptr<const Project> project);

    /**
     * @brief The project this command is part of
     */
    std::shared_ptr<const Project> project;

    /**
     * @brief The name of the command
     */
    std::string name;

    std::string cmd;

    std::vector<std::string> args;
    std::vector<std::string> prehooks;
    std::vector<std::string> posthooks;

    /**
     * @brief Runs the command
     * @details Calls the Command Handler for the used operating system
     * (currently only windows is supported) with the command.
     * @param cmd The command to run
     * @param hooks The parsed hooks for execution pre and post the command
     * @param buffer A custom buffer which handles the communication between the
     * output of the command and other parts of the application
     */
    static void run(const Command *cmd,
                    std::pair<std::vector<Hook>, std::vector<Hook>> hooks,
                    const std::shared_ptr<Buffer> buffer = nullptr);
  };

private:
  std::string groupName;
  std::vector<Command> commands;

public:
  RunGroup();

  /**
   * @brief Constructs an empty run group
   * @param groupName The name of the run group
   */
  explicit RunGroup(const std::string &groupName);

  /**
   * @brief Constructs a run group with pre defined commands
   * @param groupName The name of the run group
   * @param commands A list of commands which should be inserted on construction
   */
  explicit RunGroup(const std::string &groupName,
                    std::initializer_list<Command> commands);

  /**
   * @brief Getter for the group name
   * @return The group name
   */
  [[nodiscard]] std::string getGroupName() const;

  /**
   * @brief Get a vector of all commands which are part of this run group
   * @return A reference to the list of commands in this run group
   */
  [[nodiscard]] const std::vector<Command> &getCommands() const;

  /**
   * @brief Returns a command for an index
   * @param index The index of the command
   * @return The command at the given index
   */
  [[nodiscard]] Command getCommand(size_t index) const;

  /**
   * @brief Gets the total size of all commands in this run group
   * @return The total size of all commands in this run group
   */
  [[nodiscard]] int size() const;

  /**
   * @brief Adds a command to the rungroups commands
   * @param c The command to add
   * @return A reference to this run group
   */
  RunGroup &addCommand(Command c);

  /**
   * @brief Returns a command for an index
   * @param index The index of the command
   * @return The command at the given index
   */
  Command operator[](size_t index) const;
};

#endif // RUNGROUP_H
