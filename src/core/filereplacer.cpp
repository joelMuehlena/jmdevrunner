#include "filereplacer.h"

FileReplacer::FileReplacer() {}

void FileReplacer::replace(const Project *project,
                           const std::string &replaceArg,
                           const std::string &withArg,
                           FileReplacer::excludeVariant excludeArg) const {
  std::cout << "replacing files" << std::endl;
  std::cout << "Replacing in: " << project->getName().toStdString()
            << " Path: " << project->getLocalUrl().toStdString();
  std::cout << " Replacing: " << replaceArg << " With: " << withArg
            << std::endl;
}
