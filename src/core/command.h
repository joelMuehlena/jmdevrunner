#ifndef COMMAND_H
#define COMMAND_H

#include <array>
#include <cstdio>
#include <cstdlib>
#include <fcntl.h>
#include <io.h>
#include <iomanip>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>

#include "buffer/buffer.h"

#define BUFSIZE 256

// Forward declaration
class Project;

/**
 * @brief The base class for a command execution handler
 * @details This class provides the base for different command handlers for
 * different operating systems
 */
class Command {
protected:
  int exitStatus;
  std::shared_ptr<const Project> project;
  std::string cmd;

  // The buffer for comunicating bewtween the command output and other
  // operations e.g other threads or a gui
  std::shared_ptr<Buffer> buf;

public:
  // must not be created without any arguments like which command and on which
  // project it should be executed
  Command() = delete;

  /**
   * @brief Constructs a new Command
   * @param cmd The command which is going to be executed
   * @param project The project in which the command is going to be executed
   */
  explicit Command(std::string cmd, std::shared_ptr<const Project> project);

  /**
   * @brief Starts the executation of the command passed in the constructor
   * @details Is an abstract method because the execution depends on the using
   * operating system
   * @return the exit code of the command which was executed
   */
  virtual int exec() = 0;

  /**
   * @brief getBuffer
   * @return the buffer for communicating with the command output
   */
  std::shared_ptr<Buffer> getBuffer() const;

  /**
   * @brief Sets the buffer which is used for communicating with the command
   * output
   * @details Can be used to provide an own buffer which handles the
   * communication
   * @param buffer
   */
  void setBuffer(std::shared_ptr<Buffer> buffer);
};

#endif // COMMAND_H
