#ifndef PROJECTTYPE_H
#define PROJECTTYPE_H

#include <QDebug>
#include <QString>

#include <filesystem>
#include <iostream>
#include <string>
#include <vector>

class ProjectType {
public:
  enum class Type { NPM, YARN, GO, NOT_DEFINED };

private:
  Type type;
  bool isCompleted = false;

public:
  ProjectType();
  static std::string TypeToString(Type t);
  static std::vector<ProjectType>
  determineProjectTypes(const std::string &path);
};

#endif // PROJECTTYPE_H
