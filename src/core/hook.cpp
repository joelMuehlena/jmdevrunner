#include "hook.h"

#include <utility>

Hook::Hook(std::string name, const Project *const project)
    : hookName(std::move(name)),
      hookArgs(std::map<const std::string, Hook::mapVariantType>()),
      project(project) {}

std::string Hook::getName() const { return this->hookName; }

Hook::mapVariantType Hook::getArg(const std::string &name) const {
  auto arg = this->hookArgs.find(name);

  if (arg != this->hookArgs.end()) {
    return arg->second;
  }

  return "";
}

const std::map<const std::string, Hook::mapVariantType> &Hook::getArgs() const {
  return this->hookArgs;
}

void Hook::addArg(const std::string &name, const std::string &arg) {
  this->hookArgs.emplace(name, arg);
}

void Hook::addArg(const std::string &name, std::vector<std::string> args) {
  this->hookArgs.emplace(name, args);
}

const Project *Hook::getProjectRef() const { return this->project; }

void Hook::replaceArg(const std::string &name, const mapVariantType &newValue) {
  auto arg = this->hookArgs.find(name);

  if (arg != this->hookArgs.end()) {
    this->hookArgs.erase(arg);
    this->hookArgs.emplace(name, newValue);
  }
}
