#ifndef WINDOWSCOMMAND_H
#define WINDOWSCOMMAND_H

#ifdef _WIN32
#define NTDDI_VERSION 0x0A000008
#endif

#include <windows.h>

#include "command.h"

class WindowsCommand : public Command {
private:
  // Pseudo Console Handles
  HANDLE hPipePTYIn;
  HANDLE hPipeOut;
  HANDLE hPipeIn;
  HANDLE hPipePTYOut;

  // Console Setup functions
  void createPseudConsole(HPCON *hPc);
  void setupPseudoConsolePipes();
  HRESULT prepareStartupInformation(HPCON hpc, STARTUPINFOEXA *psi);

  /**
   * @brief Creates a windows error text from the GetLastError() WinApi
   * function.
   * @throws std::runtime_error A runtime execption if an error is catched
   * @param[out] errorText A reference to the string the error is going to be
   * applied to. The string is going to be cleared before assigned with the
   * error
   */
  void createWindowsError(const std::string &errorText);

  struct ArgsList {
    LPVOID pipe;
    Buffer *buf;
  };

  static void PipeListener(LPVOID args);
  static void PipeFeeder(LPVOID args);
  static void PipeWriterTest(LPVOID buffer);

public:
  WindowsCommand() = delete;
  explicit WindowsCommand(std::string cmd,
                          std::shared_ptr<const Project> project);

  int exec() override;
};

#endif // WINDOWSCOMMAND_H
