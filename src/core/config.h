#ifndef CONFIG_H
#define CONFIG_H

#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "filebuffer.h"
#include "project.h"

// Forward declaration
class Project;

/**
 * @brief The Config class which parses and handles the config of the
 * application
 */
class Config {
private:
  std::string pathToConfig;
  bool isOpened;
  std::unique_ptr<FileBuffer> fb;

  void refreshMarkers();

public:
  /**
   * @brief The Marker struct which represents a marker in a file
   * @details Markers are use to structure the config file
   */
  struct Marker {
    std::string name;
    int value;
    size_t pos;
    bool ingore = false;
  };

  typedef std::map<std::string, Config::Marker> markerMap;

  inline static const std::string RUNNER_FILE_OPENER = "JMRUNNER FILE v0.1";

  /**
   * @brief Holds all available markers for the config file
   */
  static inline markerMap markers = {
      std::make_pair("PROJECT_DATA_MARKER", Marker{.name = "Project Data",
                                                   .value = 0xFE23,
                                                   .pos = 0,
                                                   .ingore = false}),
      std::make_pair("END_STRING_MARKER",
                     Marker{.name = "End of a string marker",
                            .value = 0x067f,
                            .pos = 0,
                            .ingore = true})};

  /**
   * @brief Constructs a Config object
   * @param pathToConfig The path to the configuration file
   */
  explicit Config(const std::string &pathToConfig);

  /**
   * @brief Loads a marker by name of the marker map
   * @param name The name of the requested marker
   * @param isPosRelevant
   * @return An iterator to the requested marker
   */
  static markerMap::iterator loadMarker(const std::string &name,
                                        bool isPosRelevant = true);

  /**
   * @brief Opens a config file and loads the content
   */
  void open();

  /**
   * @brief Closes the loaded FileBuffer and the config data
   */
  void close();

  /**
   * @brief Getter for the internal FileBuffer
   * @return A pointer to the used filebuffer
   */
  const FileBuffer *getFileBuffer() const;

  /**
   * @brief Loads all saved projects from the config
   * @return The projects saved in the config
   */
  std::vector<std::shared_ptr<Project>> getProjectData() const;
};

#endif // CONFIG_H
