#include "bufferconsumer.h"

BufferConsumer::BufferConsumer(const size_t pos, const size_t bufSize,
                               WriteMode writeMode)
    : position(pos), bufSize(bufSize), writeMode(writeMode), writePos(0),
      readPos(0), isClosed(false) {
  this->buf = new char[this->bufSize];
  clearBuf();
}

BufferConsumer::BufferConsumer(const BufferConsumer &consumer) {
  *this = consumer;
}

BufferConsumer::~BufferConsumer() {
  delete[] this->buf;
  this->buf = nullptr;
}

BufferConsumer &BufferConsumer::operator=(const BufferConsumer &consumer) {

  if (&consumer != this) {
    this->position = consumer.position;
    this->bufSize = consumer.bufSize;
    this->writeMode = consumer.writeMode;
    this->writePos = consumer.writePos;
    this->readPos = consumer.readPos;
    this->isClosed = consumer.isClosed;

    this->buf = new char[this->bufSize];
    std::copy(consumer.buf, consumer.buf + consumer.bufSize, this->buf);
  }

  return *this;
}

void BufferConsumer::clearBuf() {
  this->readPos = this->writePos = 0;

  for (size_t i = 0; i < this->bufSize; ++i) {
    this->buf[i] = '\0';
  }
}

void BufferConsumer::terminate() { this->isClosed = true; }

bool BufferConsumer::isOpen() const { return !this->isClosed; }

void BufferConsumer::publish(char *bytes, const size_t len) {
  if (this->isClosed)
    return;

  if (len > this->bufSize)
    return;

  std::lock_guard(this->write_mu);

  if (len + this->writePos > this->bufSize &&
      this->writeMode == WriteMode::SKIP) {
    return;
  } else if (len + this->writePos > this->bufSize &&
             this->writeMode == WriteMode::OVERRIDE) {
    clearBuf();
  }

  for (size_t i = 0; i < len; ++i) {
    this->buf[this->writePos++] = bytes[i];
  }
}

void BufferConsumer::read(std::string *s) {

  while (!this->isClosed) {
    if (writePos == 0 || this->readPos == this->writePos) {
      continue;
    }

    if (this->readPos > this->bufSize || this->writePos > this->bufSize)
      throw std::runtime_error("write or read pos is bigger than buffer size");

    auto len = this->writePos - this->readPos;

    if (len <= 0)
      return;

    for (size_t i = this->readPos; i < this->readPos + len; ++i) {
      *s += this->buf[i];
    }

    if (this->readPos + 1 > this->bufSize) {
      clearBuf();
    } else {
      this->readPos += len;
    }

    if (this->writePos > this->readPos)
      continue;

    return;
  }
}
