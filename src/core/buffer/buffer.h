#ifndef BUFFER_H
#define BUFFER_H

#include <atomic>
#include <cstring>
#include <mutex>
#include <stdexcept>

#include "bufferconsumer.h"

namespace {
#define DEFAULT_WRITE_SIZE 2048
#define DEFAULT_CONSUMER_BUF_SIZE 1024
#define CONSUMER_START_SIZE 8
} // namespace

class Buffer {
private:
  BufferConsumer **consumers;
  char *writeBuffer;
  size_t writeBufferSize;
  std::atomic<size_t> writePos;
  std::atomic<size_t> lastWritePos;

  size_t consumerCount;
  size_t consumersSize;
  mutable std::mutex write_mu;

  BufferConsumer *addConsumer(const size_t &bufSize);
  void resizeConsumers(const size_t size);
  void clearWriteBuf();

  void publishWrite();

public:
  explicit Buffer(const size_t writeBufSize = DEFAULT_WRITE_SIZE);
  Buffer(const Buffer &buffer);
  ~Buffer();

  Buffer &operator=(const Buffer &buffer);

  void write(const char *bytes, const size_t len);
  BufferConsumer *
  startConsume(const size_t bufSize = DEFAULT_CONSUMER_BUF_SIZE);
  void endConsume(BufferConsumer const *const consumer);

  void close();
};

#endif // BUFFER_H
