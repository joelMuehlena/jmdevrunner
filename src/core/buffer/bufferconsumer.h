#ifndef BUFFERCONSUMER_H
#define BUFFERCONSUMER_H

#include <atomic>
#include <cstddef>
#include <limits>
#include <mutex>
#include <string>

#include <iostream>

class BufferConsumer {
public:
  enum class WriteMode { OVERRIDE, SKIP };

private:
  size_t position;
  size_t bufSize;

  WriteMode writeMode;

  char *buf;
  size_t writePos;
  size_t readPos;

  bool isClosed;

  std::mutex write_mu;

  void clearBuf();

public:
  explicit BufferConsumer(const size_t pos = 0, const size_t bufSize = 1024,
                          WriteMode writeMode = WriteMode::OVERRIDE);
  BufferConsumer(const BufferConsumer &consumer);
  ~BufferConsumer();

  BufferConsumer &operator=(const BufferConsumer &consumer);

  void publish(char *bytes, const size_t len);
  void read(std::string *s);
  void terminate();
  bool isOpen() const;
};

#endif // BUFFERCONSUMER_H
