#include "buffer.h"

Buffer::Buffer(const size_t writeBufSize)
    : writeBufferSize(writeBufSize), writePos(0), lastWritePos(0),
      consumerCount(0), consumersSize(CONSUMER_START_SIZE) {
  this->consumers = new BufferConsumer *[this->consumersSize];

  for (size_t i = 0; i < this->consumersSize; ++i) {
    this->consumers[i] = nullptr;
  }

  this->writeBuffer = new char[this->writeBufferSize];
  clearWriteBuf();
}

Buffer::Buffer(const Buffer &buffer) { *this = buffer; }

Buffer &Buffer::operator=(const Buffer &buffer) {

  if (&buffer != this) {

    std::scoped_lock(this->write_mu, buffer.write_mu);

    this->writeBufferSize = buffer.writeBufferSize;
    this->consumerCount = buffer.consumerCount;
    this->consumersSize = buffer.consumersSize;

    this->writePos = buffer.writePos.load();
    this->lastWritePos = buffer.lastWritePos.load();

    this->writeBuffer = new char[this->writeBufferSize];
    std::copy(buffer.writeBuffer, buffer.writeBuffer + buffer.writeBufferSize,
              this->writeBuffer);

    this->consumers = new BufferConsumer *[this->consumersSize];
    std::copy(buffer.consumers, buffer.consumers + buffer.consumersSize,
              this->consumers);
  }

  return *this;
}

Buffer::~Buffer() {

  this->close();

  for (size_t i = 0; i < this->consumersSize; ++i) {
    delete this->consumers[i];
  }

  delete[] this->consumers;
  delete[] this->writeBuffer;
}

void Buffer::resizeConsumers(const size_t size) {
  auto newConsumers = new BufferConsumer *[size];

  for (size_t i = 0; i < size; ++i) {
    newConsumers[i] = nullptr;
  }

  for (size_t i = 0; i < this->consumersSize; ++i) {
    newConsumers[i] = this->consumers[i];
  }

  delete[] this->consumers;
  this->consumers = newConsumers;
}

BufferConsumer *Buffer::addConsumer(const size_t &bufSize) {
  if (this->consumerCount + 1 > this->consumersSize - 2) {
    this->resizeConsumers(this->consumersSize * 2);
  }

  auto consumer = new BufferConsumer(bufSize);

  for (size_t i = 0; i < this->consumersSize; ++i) {
    if (this->consumers[i] == nullptr) {
      this->consumers[i] = consumer;
      ++this->consumerCount;
      return consumer;
    }
  }

  throw std::runtime_error("Failed to add the consumer");
}

BufferConsumer *Buffer::startConsume(const size_t bufSize) {
  auto consumer = this->addConsumer(bufSize);
  return consumer;
}

void Buffer::endConsume(const BufferConsumer *const consumer) {
  for (size_t i = 0; i < this->consumersSize; ++i) {
    if (this->consumers[i] == consumer) {
      delete this->consumers[i];
      this->consumerCount--;
      return;
    }
  }

  throw std::invalid_argument("This consumer is not part of this buffer");
}

void Buffer::clearWriteBuf() {
  this->writePos = this->lastWritePos = 0;
  for (size_t i = 0; i < this->writeBufferSize; ++i) {
    this->writeBuffer[i] = '\0';
  }
}

void Buffer::write(const char *bytes, const size_t len) {

  if (len == 0)
    return;

  if (len > this->writeBufferSize) {
    throw std::invalid_argument(
        "The buffer must not be greater than the buffer writer size");
  }

  this->write_mu.lock();

  if ((this->writePos + len) > this->writeBufferSize) {
    this->clearWriteBuf();
  }

  std::copy(bytes, bytes + len, this->writeBuffer + this->writePos);
  this->lastWritePos = this->writePos.load();
  this->writePos += len;

  this->publishWrite();

  this->write_mu.unlock();
}

void Buffer::publishWrite() {
  for (size_t i = 0; i < this->consumersSize; ++i) {
    if (this->consumers[i] != nullptr) {
      auto c = this->consumers[i];
      c->publish(this->writeBuffer + this->lastWritePos,
                 this->writePos - this->lastWritePos);
    }
  }
}

void Buffer::close() {
  for (size_t i = 0; i < this->consumersSize; ++i) {
    if (this->consumers[i] != nullptr) {
      auto c = this->consumers[i];
      c->terminate();
    }
  }
}
