#include "ui/windows/mainwindow.h"

#include <QApplication>
#include <git2cpp/initializer.h>

#include "core/hooks/filereplacerhook.h"

void setupHooks() {
  HookBase::registerHook("FileReplacer", &FileReplacerHook::build);
}

int main(int argc, char *argv[]) {
  auto_git_initializer;

  setupHooks();

  QApplication a(argc, argv);
  MainWindow w;
  w.show();

  return a.exec();
}
