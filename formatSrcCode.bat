@echo off

cd src/
for /r %t in (*.cpp *.h) do clang-format -style=LLVM -i "%t"

pause